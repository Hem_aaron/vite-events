//
//  FDataService.swift
//  Wake up at 5
//
//  Created by Hem Poudyal on 1/26/19.
//  Copyright © 2019 Hem Poudyal. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class FDataService{
    
    static var ref = Database.database().reference()
    
    static var userRef  : DatabaseReference{
        return self.ref.child("users")
    }
    
    static var purposeRef : DatabaseReference{
        return self.ref.child("purposes")
    }
    
    static var currentUserRef: DatabaseReference{
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let currentUser = self.ref.child("users").child(userID)
        return currentUser
    }
}
