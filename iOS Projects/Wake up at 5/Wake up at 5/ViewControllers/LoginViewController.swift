//
//  LoginViewController.swift
//  Wake up at 5
//
//  Created by Hem Poudyal on 1/23/19.
//  Copyright © 2019 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate{
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(false, forKey: "purposeExist")
        GIDSignIn.sharedInstance().uiDelegate = self
        signInButton.style = .wide
    }

    @IBAction func googleLoginAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }

}

