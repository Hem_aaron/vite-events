//
//  MenuViewController.swift
//  Wake up at 5
//
//  Created by Hem Poudyal on 2/3/19.
//  Copyright © 2019 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit

class MenuViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    
    
    let dataArray = ["Tips for better Sleep", "Sleep Facts", "Upgrade To Pro"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTableViewCell
        cell.lblMenu.text = dataArray[indexPath.row]
        return cell
    }
    
    
}
