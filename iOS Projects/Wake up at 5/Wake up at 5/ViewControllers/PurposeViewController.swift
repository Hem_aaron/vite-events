//
//  PurposeViewController.swift
//  Wake up at 5
//
//  Created by Hem Poudyal on 1/26/19.
//  Copyright © 2019 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class PurposeViewController: UIViewController{
    @IBOutlet weak var lblWelcome: UILabel!
    
    @IBOutlet weak var purposeField1: UITextField!
    @IBOutlet weak var purposeField2: UITextField!
    @IBOutlet weak var purposeField3: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userName = UserDefaults.standard.value(forKey: "kName") as? String{
            lblWelcome.text = "Welcome \(userName),"
        } else{
            lblWelcome.text = "Welcome"
        }
        
    }
    
    @IBAction func nextViewController(_ sender: Any) {
        let purpose1 = purposeField1.text
        let purpose2 = purposeField2.text
        let purpose3 = purposeField3.text
        
        if purpose1 != "" && purpose2 != "" && purpose3 != ""{
            let purposes: [String: AnyObject] = ["purpose1": purpose1! as AnyObject ,
                                                 "purpose2": purpose2! as AnyObject,
                                                 "purpose3": purpose3! as AnyObject
                                                ]
            
            let uid = UserDefaults.standard.value(forKey: "uid") as! String
            
            FDataService.purposeRef.child(uid).setValue(purposes)
            UserDefaults.standard.set(true, forKey: "purposeExist")
            self.performSegue(withIdentifier: "showHome", sender: nil)
        } else {
            //pop up
        }
        
    }
    
}
