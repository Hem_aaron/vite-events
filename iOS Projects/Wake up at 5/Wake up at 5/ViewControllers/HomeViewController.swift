//
//  HomeViewController.swift
//  Wake up at 5
//
//  Created by Hem Poudyal on 1/26/19.
//  Copyright © 2019 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import SVProgressHUD
import GoogleMobileAds
import UICircularProgressRing

class HomeViewController: UIViewController{
    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var lblPurpose1: UILabel!
    @IBOutlet weak var lblPurpose2: UILabel!
    @IBOutlet weak var lblPurpose3: UILabel!
    @IBOutlet weak var streakRing: UICircularProgressRing!
    var bannerView: GADBannerView!
    @IBOutlet weak var btnAlarm: UIButton!
    
    override func viewDidLoad() {
        
        self.btnAlarm.layer.cornerRadius = 8
        
        if let userName = UserDefaults.standard.value(forKey: "kName") as? String{
            lblGreeting.text = "Welcome \(userName),"
        } else{
            lblGreeting.text = "Welcome"
        }
        
        self.streakRing.maxValue = 7
        self.streakRing.value = 2
        self.streakRing.shouldShowValueText = false
        
        SVProgressHUD.show()
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        FDataService.purposeRef.child(uid).observe(.value) { (snapshot) in
            SVProgressHUD.dismiss()
            if let value = snapshot.value as? NSDictionary{
                self.lblPurpose1.text = value["purpose1"] as? String
                self.lblPurpose2.text = value["purpose2"] as? String
                self.lblPurpose3.text = value["purpose3"] as? String
            }
            
        }
        
        // In this case, we instantiate the banner with desired ad size.
//        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
//        addBannerViewToView(bannerView)
//        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
//        bannerView.rootViewController = self
//        bannerView.load(GADRequest())
        
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
}
