//
//  Persistable.swift
//  Wake up at 5
//
//  Created by Hem Poudyal on 3/11/19.
//  Copyright © 2019 Hem Poudyal. All rights reserved.
//

import Foundation

protocol Persistable {
    var ud: UserDefaults {get}
    var persistKey : String {get}
    func persist()
    func unpersist()
}
