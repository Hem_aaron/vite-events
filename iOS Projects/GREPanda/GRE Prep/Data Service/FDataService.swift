//
//  FDataService.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/18/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class FDataService {
    static var ref = Database.database().reference()
    
    static var userRef : DatabaseReference{
        return self.ref.child("users")
    }
    
    static var currentUserRef: DatabaseReference{
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let currentUser = self.ref.child("users").child(userID)
        return currentUser
    }
    
    static var vocabRef : DatabaseReference{
        return self.ref.child("vocabularies")
    }
    
    static var catRef : DatabaseReference{
        return self.ref.child("categories-Vocab")
    }
    
    static var alphaRef : DatabaseReference{
        return self.ref.child("alphabets")
    }
}
