//
//  KeysGRE.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/19/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation

let kUserName = "kUserName"
let kUserEmail = "kUserEmail"
let kUserPassword = "kUserPassword"
let kUid = "kUID"
let kCurrentCategory = "kCurrentCategory"
let kIsFromCatgories = "kIsFromCatgories"

//Firebase Database keys
let kWord = "word"
let kDefinition = "defintion"
let kSentence = "sentence"
let kSynonyms = "synonyms"
let kAlphabet = "alphabet"
let kCategoryVocab = "category"

