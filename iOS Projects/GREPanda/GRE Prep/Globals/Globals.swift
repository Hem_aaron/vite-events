//
//  Globals.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/7/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit

let userDefaultsGRE = UserDefaults.standard

func getTimeStamp() -> String{
    let timestamp = String(NSDate().timeIntervalSince1970)
    return timestamp
}

func borderView(view: UIView, cornerRadius: CGFloat, borderWidth: CGFloat, color: UIColor){
    view.layer.cornerRadius = cornerRadius
    view.layer.borderColor = color.cgColor
    view.layer.borderWidth = borderWidth
}

func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
}


func alertUser(viewController: UIViewController, title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        switch action.style{
        case .default:
            print("default")
            
        case .cancel:
            print("cancel")
            
        case .destructive:
            print("destructive")
            
            
        }}))
    viewController.present(alert, animated: true, completion: nil)
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
