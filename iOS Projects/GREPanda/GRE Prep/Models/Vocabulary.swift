//
//  Vocabulary.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/24/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class Vocabulary: Mappable{
    var category: String?
    var definition: String?
    var sentence : String?
    var synonyms : String?
    var word : String?
    var id: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        category    <- map["category"]
        definition  <- map["defintion"]
        sentence    <- map["sentence"]
        synonyms    <- map["synonyms"]
        word        <- map["word"]
        id          <- map["id"]
    }
}

