//
//  CategoryTableViewCell.swift
//  GREPanda
//
//  Created by Hem Poudyal on 5/7/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import  UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var numberBg: UIView!
    
    override func awakeFromNib() {
        borderView(view: self.numberBg, cornerRadius: 22, borderWidth: 0.4, color: UIColor.gray)
    }
    
}
