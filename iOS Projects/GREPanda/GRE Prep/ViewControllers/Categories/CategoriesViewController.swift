//
//  CategoriesViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 5/7/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit

class CategoriesViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    var catList = [String]()
    var selectedCategory = "1"
    
    override func viewDidLoad() {
        selectedCategory = userDefaultsGRE.value(forKey: kCurrentCategory) as! String
        self.catList = ["Abate - Accretion",
                        "Ablution - Adulterate",
                        "Aesthetic - Ambivalence",
                        "Ambrosia - Apathy",
                        "Apogee - Artifcat",
                        "Artless - Austere",
                        "Autonomous - Behemoth",
                        "Beneficent - Bovine",
                        "Burnish - Capricious",
                        "Captious - Caustic",
                        "Celestial - Codify",
                        "Cognizant - Condone",
                        "Congenial - Contrite",
                        "Contumacious - Craven",
                        "Credence - Demur"]
    }
    
    @IBAction func backToHome(_ sender: Any) {
        userDefaultsGRE.set(true, forKey: kIsFromCatgories)
        userDefaultsGRE.set(selectedCategory, forKey: kCurrentCategory)
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension CategoriesViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.catList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategoryTableViewCell
        cell.lblCategoryName.text = self.catList[indexPath.row]
        cell.lblNumber.text = String(indexPath.row + 1)
        let selectedCat = Int(selectedCategory)! - 1
        if indexPath.row == selectedCat{
            cell.backgroundColor = UIColor(rgb: 0x3CA485)
            cell.lblCategoryName.textColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor.white
            cell.lblCategoryName.textColor = UIColor(rgb: 0x8119A7)
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCategory = String(indexPath.row + 1)
        self.tableView.reloadData()
    }
    
    
}
