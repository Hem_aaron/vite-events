//
//  LoginViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/17/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SVProgressHUD

class LoginViewController: UIViewController{
    @IBOutlet weak var txtEmailField: UITextField!
    @IBOutlet weak var txtPasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.handleSignIn()
    }
    
    func handleSignIn(){
        guard let email = txtEmailField.text else { return }
        guard let password = txtPasswordField.text else { return }
        
        SVProgressHUD.show()
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if error == nil && authResult != nil{
                SVProgressHUD.dismiss()
                guard let user = authResult?.user else {return}
                
                //save in local memory
                if let id = user.uid as? String, let name = user.displayName as? String{
                    userDefaultsGRE.set(id, forKey: kUid)
                    userDefaultsGRE.set(name, forKey: kUserName)
                }
                userDefaultsGRE.set(email, forKey: kUserEmail)
                //Set default Id for category
                userDefaultsGRE.set("1", forKey: kCurrentCategory)
                 // show home screen
                self.performSegue(withIdentifier: "showHomeFromLogin", sender: self)
            } else {
                SVProgressHUD.dismiss()
                print("Error: \(error!.localizedDescription)")
                alertUser(viewController: self, title: "Error", message: error!.localizedDescription)
            }
        }
    }
}

