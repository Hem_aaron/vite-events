//
//  SignUpViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/17/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SVProgressHUD

class SignUpViewController: UIViewController{
    @IBOutlet weak var txtNameField: UITextField!
    @IBOutlet weak var txtEmailField: UITextField!
    @IBOutlet weak var txtPasswordField: UITextField!
    
    override func viewDidLoad() {
        
    }
    
    @IBAction func facebookLogin(_ sender: Any) {
        
    }
    
    @IBAction func googleLogin(_ sender: Any) {
        
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        self.handleSignUp()
    }
    
    
    func handleSignUp(){
        guard let name = txtNameField.text else { return }
        guard let email = txtEmailField.text else { return }
        guard let pass = txtPasswordField.text else { return }
        SVProgressHUD.show()
        
        Auth.auth().createUser(withEmail: email, password: pass) { (authResult, error) in
            if error == nil && authResult != nil {
                print("Created User")
                SVProgressHUD.dismiss()
                guard let user = authResult?.user else {return}
                
                //save name of user
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = name
                
                //save in local memory
                userDefaultsGRE.set(name, forKey: kUserName)
                userDefaultsGRE.set(email, forKey: kUserEmail)
                userDefaultsGRE.set(user.uid, forKey: kUid)
                //create firebase data
                let userData: [String: String] = ["email": email,
                                                  "username":name]
                
                changeRequest?.commitChanges(completion: { (error) in
                    if error == nil{
                        print("User display name changed.")
                        //Save in firebase
                        FDataService.userRef.child(user.uid).setValue(userData)
                        
                        //Save Category as 1 for first time
                        userDefaultsGRE.set("1", forKey: kCurrentCategory)
                        //send to home view
                        self.performSegue(withIdentifier: "showHomeView", sender: self)
                    }
                })
            } else {
                SVProgressHUD.dismiss()
                print("Error : \(error!.localizedDescription)")
                alertUser(viewController: self, title: "Error", message: error!.localizedDescription)
            }
        }
    }
    
    
}
