//
//  HomeViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/6/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import Koloda
import SVProgressHUD
import ObjectMapper
import Firebase

class HomeViewController: UIViewController{
    @IBOutlet weak var kolodaView: KolodaView!
    var vocabList = [Vocabulary]()
    var selectedVocab: Vocabulary?
    var currentCategory = "1"
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lblCatgeoryId: UILabel!
    
    override func viewDidLoad() {
        super.loadView()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        kolodaView.dataSource = self
        kolodaView.delegate = self
        currentCategory = userDefaultsGRE.value(forKey: kCurrentCategory) as! String
        self.kolodaView.isHidden = true
        loadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if currentCategory != userDefaultsGRE.value(forKey: kCurrentCategory) as! String{
            currentCategory = userDefaultsGRE.value(forKey: kCurrentCategory) as! String
             self.loadData()
        }
        self.lblCatgeoryId.text = currentCategory
    }
    
    //MARK:- Methods
    
    func loadData() {
        print(self.currentCategory)
        FDataService.catRef.child(self.currentCategory).observeSingleEvent(of: .value) { (snapshot) in
            guard snapshot.exists() else { return }
            var dictArray = [[String: AnyObject]]()
            self.vocabList.removeAll()

            for snap in snapshot.children {
                let userSnap = snap as! DataSnapshot
                let uid = userSnap.key
                
                if let dict = userSnap.value as? Dictionary<String, AnyObject>{
                    var tempDict = dict
                    dictArray.append(tempDict)
                    tempDict.updateValue(uid as AnyObject, forKey: "id")
                    if let m = Mapper<Vocabulary>().map(JSON: tempDict){
                        self.vocabList.append(m)
                    }
                }
            }
            
            let currentUid = userDefaultsGRE.value(forKey: kUid) as! String
            //Get List of words with UserRememberCount == 5 .... need to hide them, as user knows the words
        FDataService.userRef.child(currentUid).child("words").child(self.currentCategory).queryOrdered(byChild: "userRememberCount").queryEqual(toValue: 5).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                    var wordsToElimenate = [String]()
                    if let snap = snapshot.value as? NSDictionary{
                        wordsToElimenate = snap.allKeys as! [String]
                    }
                    
                    for word in wordsToElimenate{
                        let index = self.vocabList.firstIndex(where: { (vocab) -> Bool in
                            guard let value = vocab.word as? String else {return false}
                            return value == word
                        })
                        
                        if let index = index{
                            print(index)
                            self.vocabList.remove(at: index)
                        }
                    }
                    print(self.vocabList.count)
                    if self.vocabList.count > 0 {
                        self.kolodaView.isHidden = false
                        self.bgImage.isHidden = true
                    }
                    self.kolodaView.reloadData()
                } else {
                    if self.vocabList.count > 0 {
                        self.kolodaView.isHidden = false
                        self.bgImage.isHidden = true
                    }
                    //Snapshot doesn't exist
                    self.kolodaView.reloadData()
            }
            }
           
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCardDetail"{
            let vc = segue.destination as! CardDetailViewController
            vc.vocabDetail = self.selectedVocab
        }
    }
    
    //MARK:- Actions
    
    @IBAction func showMenu(_ sender: Any){
   
    }
    
    @IBAction func addData(_ sender: Any)
    {
        
    }
}

extension HomeViewController: KolodaViewDelegate{
    
    func  koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        var userRememberCount = 0
        let word = self.vocabList[index]
        let currentUid = userDefaultsGRE.value(forKey: kUid) as! String
        let userRef = FDataService.userRef.child(currentUid).child("words")
        
        //create basic Parameters to update in words
        var param : [String: Any] = ["category": word.category ?? ""]
        
        //Check if node exist and if yes get value of userRememberCount and update it
        if let wordName = word.word, let catID = word.category {
            
            userRef.child(catID).child(wordName).observeSingleEvent(of: .value) { (snapshot) in
                if snapshot.exists(){
                    if let snap = snapshot.value as? Dictionary<String, AnyObject>{
                        userRememberCount = snap["userRememberCount"] as! Int
                    }
                }
                
                //Direction control
                if direction == SwipeResultDirection.right{
                    // add to users known words list
                    if userRememberCount == 0 {
                        // if user swipes left on first, the word is 4, it appears once later
                        userRememberCount = 4
                    } else if userRememberCount < 5{
                        userRememberCount += 1
                    } else {
                        //User remembers the word!! make it hidden
                        userRememberCount = 5
                    }
                } else {
                    // add to users unknown words list
                    if userRememberCount > 0 {
                        userRememberCount -= 1
                    }
                }
                let timeStamp = NSDate().timeIntervalSince1970
                print(timeStamp)
                param.updateValue(timeStamp, forKey: "time")
                param.updateValue(userRememberCount, forKey: "userRememberCount")
                userRef.child(catID).child(wordName).setValue(param)
                userRef.child("allWords").child(wordName).setValue(param)
            }
        }
       
        
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        //show next category of words
        //koloda.reloadData()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        //show detail view
        self.selectedVocab = self.vocabList[index]
        self.performSegue(withIdentifier: "showCardDetail", sender: self)
    }
}

extension HomeViewController: KolodaViewDataSource{
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return vocabList.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        //show view components.. return a view
        let swipe = GRECardView()
        let vocab = vocabList[index]
        swipe.lblWord.text = vocab.word
        borderView(view: swipe.viewRemember, cornerRadius: 5.0, borderWidth: 0.8, color: UIColor.white)
        borderView(view: swipe.viewDontRemember, cornerRadius: 5.0, borderWidth: 0.8, color: UIColor.white)
        swipe.layer.cornerRadius = 10.0
        swipe.clipsToBounds = true
        return swipe
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.moderate
    }
    

}


