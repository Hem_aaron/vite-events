//
//  GRECardView.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/6/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import Koloda

class GRECardView: KolodaView{
    @IBOutlet weak var lblWord: UILabel!
    @IBOutlet var view: UIView!
    @IBOutlet weak var viewRemember: UIButton!
    @IBOutlet weak var viewDontRemember: UIButton!
    //    class func instanceFromNib() -> GRECardView {
//        return UINib(nibName: "GRECard", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! GRECardView
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        print("hit")
        Bundle.main.loadNibNamed("GRECard", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("GRECard", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    
    @IBAction func userRemembers(_ sender: Any) {
    }
    
    @IBAction func userDoesntRemember(_ sender: Any) {
    }
    
}
