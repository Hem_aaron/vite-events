//
//  CardDetailViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/7/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit

class CardDetailViewController: UIViewController{
    @IBOutlet weak var lblDefinitionText: UILabel!
    @IBOutlet weak var lblSynonym: UILabel!
    @IBOutlet weak var lblExample: UILabel!
    @IBOutlet weak var txtSentenceField: UITextView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var viewDefintion: UIView!
    @IBOutlet weak var viewSynonym: UIView!
    @IBOutlet weak var viewExample: UIView!
    @IBOutlet weak var viewBracket: UIView!
    
    
    @IBOutlet weak var cardStackView: UIStackView!
    var vocabDetail: Vocabulary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblDefinitionText.text = vocabDetail?.definition
        self.lblSynonym.text = vocabDetail?.synonyms
        self.lblExample.text = vocabDetail?.sentence
        
        borderView(view: viewBracket, cornerRadius: 17.5, borderWidth: 0.4, color: UIColor.lightGray)
        
        borderView(view: viewExample, cornerRadius: 5.0, borderWidth: 0.4, color: UIColor.lightGray)
        
        borderView(view: viewSynonym, cornerRadius: 5.0, borderWidth: 0.4, color: UIColor.lightGray)
        
        borderView(view: viewDefintion, cornerRadius: 5.0, borderWidth: 0.4, color: UIColor.lightGray)
    
        borderView(view: txtSentenceField, cornerRadius: 5.0, borderWidth: 0.8, color: UIColor.lightGray)
        
        borderView(view: self.cardStackView, cornerRadius: 5.0, borderWidth: 0.4, color: UIColor.lightGray)
        
        borderView(view: self.btnDone, cornerRadius: 4.0, borderWidth: 0.4, color: UIColor.lightGray)
    }
    
    @IBAction func userRemember(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
