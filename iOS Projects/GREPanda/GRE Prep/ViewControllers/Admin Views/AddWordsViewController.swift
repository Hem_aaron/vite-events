//
//  AddWordsViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/24/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit

class AddWordsViewController: UIViewController{
    
    @IBOutlet weak var txtWordField: UITextField!
    @IBOutlet weak var txtDefinitionField: UITextView!
    @IBOutlet weak var txtSynonymField: UITextField!
    @IBOutlet weak var txtSentenceField: UITextView!
    @IBOutlet weak var txtCategoryField: UITextField!
    
    
    override func viewDidLoad() {
        
    }
    
    func clearTextFields() {
        self.txtWordField.text = ""
        self.txtDefinitionField.text = ""
        self.txtSynonymField.text = ""
        self.txtSentenceField.text = ""
    }
    
    @IBAction func saveWords(_ sender: Any) {
        if txtWordField.text?.isEmpty ?? true || txtDefinitionField.text?.isEmpty ?? true || txtCategoryField.text?.isEmpty ?? true{
            alertUser(viewController: self, title: "Yo", message: "Fill all the required fields yo!")
        } else {
            let word = self.txtWordField.text!
            let alphabet = word.prefix(1)
            print(alphabet)
            print(word)
            
            FDataService.vocabRef.queryOrdered(byChild: "word").queryEqual(toValue: self.txtWordField.text).observeSingleEvent(of: .value) { (snapshot) in
                if snapshot.exists(){
                    print(snapshot)
                    alertUser(viewController: self, title: "Yo", message: "Word exists bro!!")
                } else {
                    
                    let vocabData: [String: String] = [kWord : self.txtWordField.text!,
                                                       kDefinition: self.txtDefinitionField.text!,
                                                       kSentence : self.txtSentenceField.text ?? "",
                                                       kSynonyms : self.txtSynonymField.text ?? "",
                                                       kCategoryVocab: self.txtCategoryField.text!]
                    FDataService.vocabRef.childByAutoId().setValue(vocabData){ (err, ref) in
                        guard let vocabId  = ref.key else {return}
                        //save the same data under categories
                        FDataService.catRef.child(self.txtCategoryField.text!).child(vocabId).setValue(vocabData)
                        FDataService.alphaRef.child(String(alphabet)).child(vocabId).setValue(vocabData)
                        self.clearTextFields()
                    }
                }
            }
        }
    }
    
    @IBAction func canceView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
