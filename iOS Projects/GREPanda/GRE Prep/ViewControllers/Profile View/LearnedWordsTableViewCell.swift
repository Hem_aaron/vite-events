//
//  LearnedWordsTableViewCell.swift
//  GREPanda
//
//  Created by Hem Poudyal on 5/8/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit

class LearnedWordsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblWord: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        borderView(view: self.viewBg, cornerRadius: 22.0, borderWidth: 0.5, color: UIColor.lightGray)
    }
}
