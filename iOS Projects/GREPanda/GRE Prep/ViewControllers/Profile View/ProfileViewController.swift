//
//  ProfileViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 4/14/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class ProfileViewController: UIViewController{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblWordsLearned: UILabel!
    @IBOutlet weak var lblWordsReviewed: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var allWordsView: UIView!
    @IBOutlet weak var aboutUsView: UIView!
    @IBOutlet weak var adPayView: UIView!
    
    
    var wordList = [String]()
    
    override func viewDidLoad() {
        if isKeyPresentInUserDefaults(key: kUid){
            self.lblName.text = userDefaultsGRE.object(forKey: kUserName) as? String
        }
        borderView(view: self.allWordsView, cornerRadius: 5.0, borderWidth: 0.0, color: UIColor.white)
        borderView(view: self.aboutUsView, cornerRadius: 5.0, borderWidth: 0.0, color: UIColor.white)
        borderView(view: self.adPayView, cornerRadius: 5.0, borderWidth: 0.0, color: UIColor.white)
        let currentUid = userDefaultsGRE.value(forKey: kUid) as! String
    FDataService.userRef.child(currentUid).child("words").child("allWords").queryOrdered(byChild: "userRememberCount").queryEqual(toValue: 5).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                if let snap = snapshot.value as? NSDictionary{
                    print(snap.allKeys)
                    self.wordList = snap.allKeys as! [String]
                    self.lblWordsLearned.text = String(self.wordList.count)
                }
            } else {
                self.lblWordsLearned.text = "0"
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLearnedWordsVC"{
            let vc = segue.destination as! LearnedWordsViewController
            vc.wordList = self.wordList
        }
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func showLearnedWords(_ sender: Any) {
        if self.wordList.count != 0{
            self.performSegue(withIdentifier: "showLearnedWordsVC", sender: self)

        }
    }
    
    @IBAction func showAllWords(_ sender: Any) {
        self.performSegue(withIdentifier: "showAllWords", sender: self)
    }
    
    
}
