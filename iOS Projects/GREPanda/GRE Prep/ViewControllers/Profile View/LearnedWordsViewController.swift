//
//  LearnedWordsViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 5/8/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class LearnedWordsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var wordList = [String]()
    
    override func viewDidLoad() {
        self.tableView.reloadData()
    }
    
    @IBAction func backToList(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension LearnedWordsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.wordList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LearnedWordsTableViewCell
        cell.lblNumber.text =  String(indexPath.row + 1)
        cell.lblWord.text = self.wordList[indexPath.row]
        return cell
    }
    
    
}
