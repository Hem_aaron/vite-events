//
//  AllWordsViewController.swift
//  GREPanda
//
//  Created by Hem Poudyal on 5/15/19.
//  Copyright © 2019 HemPoudyal. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import FirebaseDatabase
import ObjectMapper

class AllWordsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var wordList = [String]()
    
    override func viewDidLoad() {
        FDataService.vocabRef.observeSingleEvent(of: .value) { (snapshot) in
        guard snapshot.exists() else { return }
            
        for snap in snapshot.children {
            let userSnap = snap as! DataSnapshot
 
            if let dict = userSnap.value as? Dictionary<String, AnyObject>{
            var tempDict = dict
                if let word = tempDict["word"] as? String{
                    self.wordList.append(word)
                }
            }
        }
            self.tableView.reloadData()
        }
    }
    
    @IBAction func backToList(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AllWordsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.wordList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LearnedWordsTableViewCell
        cell.lblNumber.text =  String(indexPath.row + 1)
        cell.lblWord.text = self.wordList[indexPath.row]
        return cell
    }
    
    
}
