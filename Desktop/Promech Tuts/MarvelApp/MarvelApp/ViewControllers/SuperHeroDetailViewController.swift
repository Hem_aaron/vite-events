//
//  SuperHeroDetailViewController.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/20/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import SVProgressHUD

class SuperHeroDetailViewController: UIViewController {
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgSuperHero: UIImageView!
    @IBOutlet weak var lblEventAlert: UILabel!
    
    var marvelHero: MarvelCharacters?
    var eventArray =  [EventOfCharacters]()
    
    override func viewDidLoad() {
        self.loadData()
    }
    
    func loadData(){
        guard let data = marvelHero else {return}
        self.title = data.name
        
        if data.descriptionCharacter!.isEmpty{
            txtDescription.text = "Description not available."
        } else {
            txtDescription.text = data.descriptionCharacter
        }
        
        if let imageUrl = data.thumbnail?.path{
            if let ext = data.thumbnail?.extensionImg{
                let imgHero = imageUrl + "." + ext
                imgSuperHero.sd_setImage(with: URL(string: imgHero))
            }
        }
        
        
        SVProgressHUD.show()
        FetchDataService().getEventDetail((marvelHero?.id)!, successBlock: {
            (eventArr) in
            self.eventArray = eventArr
            SVProgressHUD.dismiss()
            self.tableView.reloadData()
            
            if self.eventArray.count > 0{
                self.lblEventAlert.text = ""
            } else {
                self.lblEventAlert.text = "Sorry, no events for this character."
            }
            
        }) { (msg) in
            SVProgressHUD.dismiss()
        }
    }
    
}

extension SuperHeroDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = eventArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = event.title
        if let imageUrl = event.thumbnail?.path{
            if let ext = event.thumbnail?.extensionImg{
                let imageString = imageUrl + "." + ext
                cell.imageView?.sd_setImage(with: URL(string:imageString))
            }
        }
        return cell
        
    }
    
    
}
