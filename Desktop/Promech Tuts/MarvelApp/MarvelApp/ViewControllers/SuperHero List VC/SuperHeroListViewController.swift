//
//  SuperHeroListViewController.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/18/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage


class SuperHeroListViewController: UIViewController {
    var charMarvelArray : [MarvelCharacters] = []
    
    override func viewDidLoad() {
        
    }
}

extension SuperHeroListViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.charMarvelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let character = self.charMarvelArray[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SuperHeroCollectionViewCell
        cell.lblTitle.text = character.name
        if let imageUrl = character.thumbnail?.path{
            if let ext = character.thumbnail?.extensionImg{
                let imageString = imageUrl + "." + ext
                cell.imgThumbnail.sd_setImage(with: URL(string:imageString))
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let marvelHero = charMarvelArray[indexPath.row]
        
        //Navigate to SuperHeroDetail on selection
        let superHeroListVC = self.storyboard?.instantiateViewController(withIdentifier: "SuperHeroDetailVC") as! SuperHeroDetailViewController
        superHeroListVC.marvelHero = marvelHero
        self.navigationController?.pushViewController(superHeroListVC, animated: true)
    }
    
    
}

