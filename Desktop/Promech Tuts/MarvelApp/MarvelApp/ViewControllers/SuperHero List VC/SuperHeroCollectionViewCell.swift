//
//  SuperHeroTableViewCell.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/19/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
    
    
}
