//
//  HomeViewController.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/18/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import CryptoSwift

class HomeViewController: UIViewController {
    @IBOutlet weak var textFieldSuperHero: UITextField!
    
    override func viewDidLoad() {

    }
    
    @IBAction func showSuperHeroList(_ sender: Any) {
        SVProgressHUD.show()
        self.loadData()
    }
    
    
    func loadData(){
        
        if textFieldSuperHero.text?.isEmpty ?? true {
            SVProgressHUD.dismiss()
            return
        } else {

            FetchDataService().getCharacterDetail(textFieldSuperHero.text!, successBlock: { (characterDetail) in
                SVProgressHUD.dismiss()
                
                //Navigate to SuperHero Listing on success
                let superHeroListVC = self.storyboard?.instantiateViewController(withIdentifier: "superHeroListVC") as! SuperHeroListViewController
                superHeroListVC.charMarvelArray = characterDetail
                self.navigationController?.pushViewController(superHeroListVC, animated: true)
                
            }) { (msg) in
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
}
