//
//  Globals.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/20/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

func getTimeStamp() -> String{
    let timestamp = String(NSDate().timeIntervalSince1970)
    return timestamp
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}

