//
//  Webservice.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/17/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import Alamofire

//Create your profile on MarvelAPI to get the key
let publicKey = ""
let privateKey = ""

let kBaseURL = "https://gateway.marvel.com:443/v1/public/characters"

enum networkStatus: Int{
    case success = 200
    case failure = 417
    case badRequest = 400
    case unAuthorized = 401
}


class WebService: NSObject{
    
    class func getAt(
        _ url:String,
        parameters: [String: AnyObject],
        successBlock:@escaping ((_ response: DataResponse<Any>) -> Void),
        failureBlock:@escaping ((_ message:String) -> Void)) {
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responses) in
            
            guard let response = responses.result.value as? [String: AnyObject] else{
                return
            }
            let statusCode = response["code"] as? Int
            if statusCode == networkStatus.success.rawValue{
                successBlock(responses)
            } else {
                failureBlock("Failed to get data")
            }
        }

    }
    
    class func getMarvelData(_ url:String,
                            parameters:[String: AnyObject],
                            successBlock:@escaping ((_ responseArray: [[String: AnyObject]]) -> Void),
                            failureBlock:@escaping ((_ message:String) -> Void)) {
        
        //hash value : md5(ts+privateKey+publicKey)
        //Timestamp and hashValue should have same time
        let ts = getTimeStamp()
        let hashString = ts + privateKey + publicKey
        let hashValue = hashString.md5()
        
        
        var param : [String : Any] = [
            "apikey":publicKey ,
            "ts": ts,
            "hash": hashValue ]
        
        if !parameters.isEmpty {
            param.update(other: parameters)
        }
        
        WebService.getAt(url, parameters: param as [String : AnyObject], successBlock: { (responses) in
            guard let response = responses.result.value as? [String: AnyObject] else{
                return
            }
            
            guard let data = response["data"] as? [String: AnyObject] else {
                return
            }
            
            guard let resultArray = data["results"] as? [[String: AnyObject]] else {
                return
            }
            
            successBlock(resultArray)
        }) { (msg) in
                failureBlock(msg)
        }
    }
}
