//
//  FetchDataService.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/20/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import ObjectMapper

class FetchDataService {
    
    func getCharacterDetail(_ nameStartsWith:String,
                       successBlock:@escaping (([MarvelCharacters]) -> ()),
                       failure:@escaping ((_ message:String) -> ())) {
        
        let param : [String : Any] = [
            "nameStartsWith": nameStartsWith]
        
        WebService.getMarvelData(kBaseURL, parameters: param as [String : AnyObject], successBlock: { (responseArray) in
            
            var charArray = [MarvelCharacters]()
           for result in responseArray {
                if let m = Mapper<MarvelCharacters>().map(JSON: result) {
                     charArray.append(m)
                }
            }
            successBlock(charArray)
            
        }) { (message) in
            
        }
    }
    
    func getEventDetail(_ characterId:Int,
                            successBlock:@escaping (([EventOfCharacters]) -> ()),
                            failure:@escaping ((_ message:String) -> ())) {
        
        let param : [String : Any] = [
            "characterId": characterId]
        
        let eventURL = kBaseURL + "/" + String(characterId) + "/events"
        
        WebService.getMarvelData(eventURL, parameters: param as [String : AnyObject], successBlock: { (responseArray) in
            
            var eventArray = [EventOfCharacters]()
            for result in responseArray {
                if let m = Mapper<EventOfCharacters>().map(JSON: result) {
                    eventArray.append(m)
                }
            }
            successBlock(eventArray)
            
        }) { (message) in
            
        }
    }
    
    

}
