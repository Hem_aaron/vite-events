//
//  EventsOfCharactersModel.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/20/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import ObjectMapper

class EventOfCharacters: Mappable{
    var title : String?
    var description : String?
    var thumbnail: Thumbnail?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        title         <- map["title"]
        description   <- map["description"]
        thumbnail     <- map["thumbnail"]
    }
}
