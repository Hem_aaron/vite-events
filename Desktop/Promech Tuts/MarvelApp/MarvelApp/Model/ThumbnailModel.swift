//
//  ThumbnailModel.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/25/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import ObjectMapper

class Thumbnail: Mappable{
    var extensionImg: String?
    var path: String?
    var imgURL: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        extensionImg  <- map["extension"]
        path        <- map["path"]
    }
}
