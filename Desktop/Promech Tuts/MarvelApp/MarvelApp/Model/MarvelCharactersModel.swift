//
//  MarvelCharactersModel.swift
//  MarvelApp
//
//  Created by Hem Poudyal on 12/18/18.
//  Copyright © 2018 Hem Poudyal. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

//struct MarvelCharacters{
//    var id: Int?
//    var name: String?
//    var descriptionCharacter: String?
//    var thumbnail: String?
//    var events : [EventOfCharacters]?
//}


class MarvelCharacters: Mappable{
    var id: Int?
    var name: String?
    var descriptionCharacter: String?
    var thumbnail: Thumbnail?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id                      <- map["id"]
        name                    <- map["name"]
        descriptionCharacter    <- map["description"]
        thumbnail               <- map["thumbnail"]
    }
}


